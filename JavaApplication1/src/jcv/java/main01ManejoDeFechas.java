/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.java;

import java.util.Calendar;
import java.util.Date;

/**
 * @author jovac Manejo de Fechas (Date y Calendar)
 */
public class main01ManejoDeFechas {

    public static void main(String[] args) {
        Date fecha = new Date();
        int anio = fecha.getYear();
        int day= fecha.getDate();
        System.out.println("El año en curso es " + (anio+1900));
        System.out.println("El dia de hoy es "+day);
        /* Clase Calendar */
        Calendar calendario= Calendar.getInstance();
        int anio2= calendario.get(Calendar.YEAR);
        
        System.out.println("(Clase Calendar)El año en curso es "+anio2);
    }
}
