/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.java;

/**
 *
 * @author jovac
 */
public class main03Excepciones {
    public static void main(String[] args) {
        
        try {
            //intentar proceso 1
            main03Excepciones obj= new main03Excepciones();
            obj.proceso1(7, 0);
        } catch (Exception e) {
            //manejo de la excepcion
            System.out.println(e.getMessage());
        } finally{
            //se ejecuta si o si
            System.out.println("Bloque FINALLY");
        }
    }
    public void proceso1(int a, int b)
    {
        int division= a/b;
    }
}
