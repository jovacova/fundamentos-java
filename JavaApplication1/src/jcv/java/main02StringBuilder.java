/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.java;

/**
 *
 * @author jovac
 */
public class main02StringBuilder {
    //Concatenaciones de String, mejora tiempos
    public static void main(String[] args) {
        String texto = "Hola soy jovacova";
        StringBuilder textoBuilder = new StringBuilder();

        texto += "Bitbucket";
        textoBuilder.append("Hola soy jovacova ").append("Bitbucket 2 ").append("con");
        System.out.println(textoBuilder.toString());
        
    }
}
