/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jovac
 */
public class ListarDB {
    public void listar() throws SQLException {
        //JDBC driver nombre y base de datos URL
        final String JDBC_DRIVER = "org.postgresql.Driver";//com.mysql.jdbc.Driver
        final String DB_URL = "jdbc:postgresql://localhost:5532/NombreDeLaBaseDatos";
        //jdbc:mysql://localhost/NombreDeLaBaseDatos;

        //Base de Datos credenciales
        final String USER = "postgres";
        final String PASSWORD = "root";

        Connection conexion = null;
        try {
            Class.forName(JDBC_DRIVER);
            conexion = DriverManager.getConnection(DB_URL, USER, PASSWORD);

            PreparedStatement st = conexion.prepareStatement("SELECT * FROM PERSONA");
            ResultSet rs=st.executeQuery();
            
            while(rs.next())
            {
                String a= rs.getString("nombre");
                System.out.println("nombre : "+a);
            }
            rs.close();
            st.close();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (conexion != null) {
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        }
    }
}
