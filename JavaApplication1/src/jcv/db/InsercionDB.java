/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author jovac
 */
public class InsercionDB {

    public static void main(String[] args) throws SQLException {
        InsercionDB obj = new InsercionDB();
        String valor = obj.solicitarValores();
        if (valor != null) {
            obj.registrarBD(valor);
        }
    }

    public void registrarBD(String valor) throws SQLException {
        //JDBC driver nombre y base de datos URL
        final String JDBC_DRIVER = "org.postgresql.Driver";//com.mysql.jdbc.Driver
        final String DB_URL = "jdbc:postgresql://localhost:5532/NombreDeLaBaseDatos";
        //jdbc:mysql://localhost/NombreDeLaBaseDatos;

        //Base de Datos credenciales
        final String USER = "postgres";
        final String PASSWORD = "root";

        Connection conexion = null;
        try {
            Class.forName(JDBC_DRIVER);
            conexion = DriverManager.getConnection(DB_URL, USER, PASSWORD);

            PreparedStatement st = conexion.prepareStatement("INSERT INTO PERSONA(nombre) values(?)");
            st.setString(1, valor);

            st.executeUpdate();
            st.close();
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (conexion != null) {
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        }
    }

    public String solicitarValores() {
        Scanner sc = new Scanner(System.in);
        String nombre = sc.next();
        return nombre;
    }
}
