/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcv.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author jovac
 */
public class ConexionDB {

    public static void main(String[] args) throws SQLException {
        //JDBC driver nombre y base de datos URL
        final String JDBC_DRIVER = "org.postgresql.Driver";//com.mysql.jdbc.Driver
        final String DB_URL = "jdbc:postgresql://localhost:5532/NombreDeLaBaseDatos";
        //jdbc:mysql://localhost/NombreDeLaBaseDatos;
        
        //Base de Datos credenciales
        final String USER= "postgres";
        final String PASSWORD="root";
        
        Connection conexion =null;
        try {
            Class.forName(JDBC_DRIVER);
            conexion= DriverManager.getConnection(DB_URL, USER, PASSWORD);
            
            PreparedStatement st= conexion.prepareStatement("INSERT INTO PERSONA(nombre) values('jovacova BITBUCKET')");
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
            e.getMessage();
        }finally{
            if(conexion!=null){
                if (!conexion.isClosed()) {
                    conexion.close();
                }
            }
        }
    }
}
